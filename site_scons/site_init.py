def tools_that_exist(tools):
    """
    SCons barfs when trying to create a tool it doesn't know about. Filter
    those out.
    """
    res = []
    for tool in tools:
        if isinstance(tool, str):
            try:
                res.append(Tool(tool))
            except:
                pass
        else:
            res.append(tool)

    return res

# A utility class for a list of alternative tools.
class AlternativeTools:
    def __init__(self, *tools):
        self.tools = tools_that_exist(tools)

    def exists(self, *args, **kwargs):
        return any(tool.exists(*args, **kwargs) for tool in self.tools)

    def __call__(self, *args, **kwargs):
        for tool in self.tools:
            if tool.exists(*args, **kwargs):
                return tool(*args, **kwargs)
        print("%s: no alternative found." % (self,))

    def __str__(self):
        return " | ".join(str(tool) for tool in self.tools)

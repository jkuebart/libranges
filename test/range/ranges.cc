#include "rng.h"

#include <array>
#include <vector>

#define BOOST_TEST_MODULE ranges test
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

using rng::Take;
using rng::all;
using rng::as;
using rng::enumerate;
using rng::nest;
using rng::primes;
using rng::size;
using rng::zip;

BOOST_AUTO_TEST_CASE(test_all)
{
    std::vector const expected{1, 2, 3};
    BOOST_TEST(expected == (all({1, 2, 3}) | as<decltype(expected)>));
}

BOOST_AUTO_TEST_CASE(test_all_unequal)
{
    std::vector const expected{1, 2, 4};
    BOOST_TEST(expected != (all({1, 2, 3}) | as<decltype(expected)>));
}

BOOST_AUTO_TEST_CASE(test_enumerate)
{
    std::array const a0{"alpha", "beta", "gamma"};
    std::vector const expected{std::pair{0, "alpha"}, std::pair{1, "beta"}, std::pair{2, "gamma"}};
    BOOST_TEST(expected == (enumerate(all(a0)) | as<decltype(expected)>));
}

BOOST_AUTO_TEST_CASE(test_zip)
{
    std::array const a0{1, 2, 3};
    std::array const a1{'a', 'b', 'c'};
    std::vector const expected{std::pair{1, 'a'}, std::pair{2, 'b'}, std::pair{3, 'c'}};
    BOOST_TEST(expected == (zip(all(a0), all(a1)) | as<decltype(expected)>));
}

BOOST_AUTO_TEST_CASE(test_nest)
{
    std::array const a0{"alpha", "beta"};
    std::array const a1{1, 2, 3};
    std::array const a2{'a', 'b'};
    BOOST_TEST((nest(all(a0), all(a1), all(a2)) | size) == a0.size() * a1.size() * a2.size());

    std::vector const expected{
        std::make_tuple("alpha", 1, 'a'),
        std::make_tuple("beta", 1, 'a'),
        std::make_tuple("alpha", 2, 'a'),
        std::make_tuple("beta", 2, 'a'),
        std::make_tuple("alpha", 3, 'a'),
        std::make_tuple("beta", 3, 'a'),
        std::make_tuple("alpha", 1, 'b'),
        std::make_tuple("beta", 1, 'b'),
        std::make_tuple("alpha", 2, 'b'),
        std::make_tuple("beta", 2, 'b'),
        std::make_tuple("alpha", 3, 'b'),
        std::make_tuple("beta", 3, 'b'),
    };
    BOOST_TEST(expected == (nest(all(a0), all(a1), all(a2)) | as<decltype(expected)>));
}

BOOST_AUTO_TEST_CASE(test_mutation)
{
    std::array a0{1, 2, 3, 4, 5};
    for (auto rng{enumerate(all(a0) | Take{3})}; !rng.empty(); rng.popFront()) {
            // Multiply each element by its index.
            std::get<1>(rng.front()) *= std::get<0>(rng.front());
        }

    std::array const expected{0, 2, 6, 4, 5};
    BOOST_TEST(expected == a0);
}

BOOST_AUTO_TEST_CASE(test_primes)
{
    std::vector const expected{2, 3, 5, 7, 11, 13, 17, 19, 23};
    BOOST_TEST(expected == (primes() | Take{expected.size()} | as<decltype(expected)>));
}

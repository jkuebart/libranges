A C++ ranges library
====================

This library is primarily based on Andrei Alexandresu's article [Iterators
must go][0]. It is mainly concerned with simplicity of expression and
flexibility, and less on being all-encompassing or extreme performance. If
pointers, iterators or the std:: algorithms work well, by all means they
should be used.

But there are several cases where having to deal with iterator pairs just
to describe a simple range concept is clumsy. For example, a function
expecting a pair of iterators can't accept the result of another function.

Ranges offer a very attractive alternative, and that's what this toolkit
attempts to provide.


State of the API
----------------

The API must be considered completely unstable. For example, there is
currently a `sort` filter which collects the input range in a
`std::vector`, sorts it and returns an `OwningRange`. At the very least
there will need to be a `sort` function to sort ranges in place.

Additionally, the code currently only provides one-pass ranges. In the long
run, forward, double-ended and random-access ranges need to be added and
algorithms specialised properly. All of this is very much a work in
progress.


Building and testing
--------------------

The source code can be built using [SCons][SCONS].

    scons

Files are built in subdirectories of `.obj`.

The build mode can be specified as `release` (the default) or `debug` on
the command line. Multiple build modes can be given as one or more `MODES`
arguments.

    scons MODES=debug

The target `test` runs the tests:

    scons MODES=all test

In order to clean the `.obj` directory, use

    scons -c MODES=all


[0]: http://www.informit.com/articles/article.aspx?p=1407357
[SCONS]: https://scons.org/

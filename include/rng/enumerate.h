#ifndef RNG_ENUMERATE_H
#define RNG_ENUMERATE_H

#include "rng/zip.h"

#include <cstddef>

namespace rng {
    namespace detail {
        /**
         * A range that produces all indices.
         */
        class IndexRange {
        public:
            constexpr IndexRange()
            : mIndex(0)
            {}

            constexpr bool empty() const {
                return false;
            }

            constexpr void popFront() {
                ++mIndex;
            }

            constexpr std::size_t front() const {
                return mIndex;
            }

        private:
            std::size_t mIndex;
        };
    }

    /**
     * Return a range of tuples with the index as the first element.
     */
    template<typename Range>
    constexpr auto enumerate(Range const& range) {
        return rng::zip(detail::IndexRange(), range);
    }
}

#endif // RNG_ENUMERATE_H

#ifndef RNG_STREAMER_H
#define RNG_STREAMER_H

#include "rng/all.h"
#include "rng/range.h"

#include <ios>
#include <iostream>
#include <string>
#include <string_view>
#include <utility>

namespace rng
{
    inline std::istream& consume(std::istream& is, std::string_view const& str)
    {
        if (!is || str.empty()) {
            return is;
        }

        std::string dummy(str.size(), 0);

        is.read(&dummy[0], static_cast<std::streamsize>(dummy.size()));
        if (dummy != str) {
            throw std::ios_base::failure(
                "Expected '" + std::string{str} + "', got '" + dummy + "'"
            );
        }

        return is;
    }

    template<typename Range>
    struct Streamer
    {
        friend std::istream& operator >>(std::istream& is, Streamer&& rhs)
        {
            consume(is, rhs.left);

            std::string_view sp{};
            for (auto rng{rhs.range}; is && !rng.empty(); rng.popFront()) {
                consume(is, sp);
                is >> rng.front();
                sp = rhs.sep;
            }

            return consume(is, rhs.right);
        }

        friend std::ostream& operator <<(std::ostream& os, Streamer const& rhs)
        {
            os << rhs.left;
            std::string_view sp{};
            for (auto const& elem : rng::Range{rhs.range}) {
                os << sp << elem;
                sp = rhs.sep;
            }
            return os << rhs.right;
        }

        Range const range;
        std::string_view const sep;
        std::string_view const left;
        std::string_view const right;
    };

    template<typename Range>
    Streamer<Range> streamRange(
        Range range,
        std::string_view const& sep = " ",
        std::string_view const& left = {},
        std::string_view const& right = {}
    )
    {
        return Streamer<Range>{std::move(range), sep, left, right};
    }

    template<typename From, typename To>
    auto streamFromTo(
        From from,
        To to,
        std::string_view const& sep = " ",
        std::string_view const& left = {},
        std::string_view const& right = {}
    )
    {
        return streamRange(
            rng::IteratorRange{std::move(from), std::move(to)},
            sep,
            left,
            right
        );
    }

    template<typename Container>
    auto stream(
        Container&& cont,
        std::string_view const& sep = " ",
        std::string_view const& left = {},
        std::string_view const& right = {}
    )
    {
        return streamRange(
            rng::all(std::forward<Container>(cont)),
            sep,
            left,
            right
        );
    }
} // namespace rng

#endif // RNG_STREAMER_H

#ifndef RNG_STREAM_H
#define RNG_STREAM_H

#include "tpl/output.h"

#include <iosfwd>

namespace rng {
    namespace detail {
        template<typename Range>
        class streamer {
        public:
            explicit constexpr streamer(Range const& range)
            : mRange(range)
            {}

            template<typename Char, typename Traits>
            friend constexpr std::basic_ostream<Char, Traits>& operator <<(
                std::basic_ostream<Char, Traits>& os, streamer const& s
            ) {
                using tpl::operator<<;

                os << '[';
                char const* sep{""};
                for (auto rng = s.mRange; !rng.empty(); rng.popFront()) {
                    os << sep << rng.front();
                    sep = ", ";
                }
                return os << ']';
            }

        private:
            Range const mRange;
        };
    }

    /**
     * Range output.
     */
    template<typename Range>
    constexpr detail::streamer<Range> stream(Range const& range) {
        return detail::streamer<Range>(range);
    }
}

#endif // RNG_STREAM_H

#ifndef RNG_PRIMES_H
#define RNG_PRIMES_H

#include "rng/anyOf.h"

#include <vector>

namespace rng
{
    /**
     * A range of all prime numbers.
     */
    class primes
    {
    public:
        constexpr bool empty() const
        {
            return false;
        }

        constexpr void popFront()
        {
            unsigned p = mPrimes.back();
            bool divisible = true;
            while (divisible) {
                // Test the next odd number.
                p = (1 + p) | 1;
                divisible =
                    all(mPrimes) |
                    AnyOf{
                        [&p](unsigned const d)
                        {
                            return 0 == p % d;
                        }
                    };
            }
            mPrimes.emplace_back(p);
        }

        constexpr unsigned front() const
        {
            return mPrimes.back();
        }

    private:
        std::vector<unsigned> mPrimes{2};
    };
}

#endif // RNG_PRIMES_H

#ifndef TPL_STREAMER_H
#define TPL_STREAMER_H

#include "tpl/each.h"
#include "tpl/output.h"

#include <ostream>
#include <string_view>
#include <tuple>
#include <utility>

namespace tpl
{

/**
 * A Streamer can capture an rvalue reference (for streaming results),
 * an lvalue reference (for reading input) or a const lvalue reference for
 * unmodifiable lvalues.
 */

    template<typename Tuple>
    struct Streamer
    {
        friend std::ostream& operator<<(
            std::ostream& os,
            Streamer const& rhs
        )
        {
            os << rhs.left;
            std::string_view sp{};
            tpl::each(
                rhs.tuple,
                [&os, &rhs, &sp](auto const& elem) {
                    os << sp << elem;
                    sp = rhs.sep;
                }
            );
            return os << rhs.right;
        }

        Tuple&& tuple;
        std::string_view const sep;
        std::string_view const left;
        std::string_view const right;
    };

/*
 * We need an overload for every reference type to beat the forwarding
 * reference above.
 */

    template<typename ...Tuple>
    auto stream(
        std::tuple<Tuple...> const& tuple,
        std::string_view const& sep = " ",
        std::string_view const& left = {},
        std::string_view const& right = {}
    )
    {
        return Streamer<std::tuple<Tuple...> const&>{
            tuple,
            sep,
            left,
            right
        };
    }

    template<typename T, typename U>
    auto stream(
        std::pair<T, U> const& p,
        std::string_view const& sep = " ",
        std::string_view const& left = {},
        std::string_view const& right = {}
    )
    {
        return Streamer<std::pair<T, U> const&>{p, sep, left, right};
    }

    template<typename ...Tuple>
    auto stream(
        std::tuple<Tuple...>& tuple,
        std::string_view const& sep = " ",
        std::string_view const& left = {},
        std::string_view const& right = {}
    )
    {
        return Streamer<std::tuple<Tuple...>&>{tuple, sep, left, right};
    }

    template<typename T, typename U>
    auto stream(
        std::pair<T, U>& p,
        std::string_view const& sep = " ",
        std::string_view const& left = {},
        std::string_view const& right = {}
    )
    {
        return Streamer<std::pair<T, U>&>{p, sep, left, right};
    }

    template<typename ...Tuple>
    auto stream(
        std::tuple<Tuple...>&& tuple,
        std::string_view const& sep = " ",
        std::string_view const& left = {},
        std::string_view const& right = {}
    )
    {
        return Streamer<std::tuple<Tuple...>>{
            std::move(tuple),
            sep,
            left,
            right
        };
    }

    template<typename T, typename U>
    auto stream(
        std::pair<T, U>&& p,
        std::string_view const& sep = " ",
        std::string_view const& left = {},
        std::string_view const& right = {}
    )
    {
        return Streamer<std::pair<T, U>>{std::move(p), sep, left, right};
    }
} // namespace tpl

#endif // TPL_STREAMER_H

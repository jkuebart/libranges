#ifndef TPL_ACCUMULATE_RIGHT_H
#define TPL_ACCUMULATE_RIGHT_H

#include <tuple>

namespace tpl
{
    namespace detail
    {

// Unlike accumulate(), Initial here never takes function return values, so
// any rvalue reference was originally passed in and is safe to return.

        template<typename Func, typename Initial>
        constexpr decltype(auto) accumulateRightImpl(Func, Initial&& initial)
        {
            return std::forward<Initial>(initial);
        }

        template<typename Func, typename Initial, typename Arg0, typename ...Tuple>
        constexpr decltype(auto) accumulateRightImpl(
            Func func,
            Initial&& initial,
            Arg0&& arg0,
            Tuple&& ...tuple
        )
        {
            return func(
                accumulateRightImpl(func, std::forward<Initial>(initial), std::forward<Tuple>(tuple)...),
                std::forward<Arg0>(arg0)
            );
        }
    } // namespace tpl::detail

    template<typename Tuple, typename Func>
    constexpr decltype(auto) accumulateRight(Tuple&& tuple, Func func)
    {
        return std::apply(
            [&func](auto&& ...tpl) {
                return detail::accumulateRightImpl(func, std::forward<decltype(tpl)>(tpl)...);
            },
            std::forward<Tuple>(tuple)
        );
    }

    template<typename Tuple, typename Initial, typename Func>
    constexpr decltype(auto) accumulateRight(Tuple&& tuple, Initial&& initial, Func func)
    {
        return std::apply(
            [&func, &initial](auto&& ...tpl) {
                return detail::accumulateRightImpl(
                    func,
                    std::forward<Initial>(initial),
                    std::forward<decltype(tpl)>(tpl)...
                );
            },
            std::forward<Tuple>(tuple)
        );
    }
} // namespace tpl

#endif // TPL_ACCUMULATE_RIGHT_H
